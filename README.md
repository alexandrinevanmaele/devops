# Devops

# Networking 101


# I/ Création du réseau personalisé 

```bash
gcloud compute networks create taw-custom-network --subnet-mode custom
```

# II/ Définition de la région et de la zone

Afin de garantir le déploiement approprié de nos ressources dans des régions et zones géographiques adaptées, nous préconfigurons ces paramètres. Cette action vise à améliorer la performance en termes de latence et à optimiser les coûts.

# 1.	Create subnet-europe-west1 with an IP prefix:

```bash
 gcloud compute networks subnets create subnet-europe-west1 \
   --network taw-custom-network \
   --region europe-west1 \
   --range 10.0.0.0/16
```
# 2.	Create subnet-europe-west4 with an IP prefix:

```bash
gcloud compute networks subnets create subnet-europe-west4 \
   --network taw-custom-network \
   --region europe-west4 \
   --range 10.1.0.0/16
```

# 3.	Create subnet-us-east1 with an IP prefix:

```bash
gcloud compute networks subnets create subnet-us-east1 \
   --network taw-custom-network \
   --region us-east1 \
   --range 10.2.0.0/16
```

# III/ Paramétrage des règles de pare-feu
Afin de renforcer la sécurité de notre réseau et de superviser l'accès aux instances VM, nous mettons en place des règles de pare-feu. Dans cet exemple, une règle est instaurée pour autoriser le trafic HTTP entrant.

```bash
gcloud compute firewall-rules create nw101-allow-http \--allow tcp:80 --network taw-custom-network --source-ranges 0.0.0.0/0 \--target-tags http
```

# IV/ Installation des instances VM

En dernière étape, nous déployons des instances VM au sein de notre réseau, en les positionnant dans les sous-réseaux adéquats. Ces instances joueront le rôle d'hôtes pour nos applications et services.

```bash
gcloud compute instances create us-test-01 \--subnet subnet-europe-west1 \--zone europe-west1-b \--machine-type e2-standard-2 \--tags ssh,http,rules
```

# V/ Ajout de regle supplémentaire : 

Pour renforcer la sécurité de manière exhaustive, il est essentiel d'incorporer des règles de pare-feu supplémentaires. Ces règles devraient autoriser le trafic ICMP (pour le ping), SSH (pour établir des connexions sécurisées avec nos instances), et éventuellement RDP (pour l'accès à distance sur des systèmes Windows).

# 1. ICMP 

```bash
gcloud compute firewall-rules create "nw101-allow-icmp" --allow icmp --network "taw-custom-network" --target-tags rules
```

# 2. Internal Communication

```bash
gcloud compute firewall-rules create "nw101-allow-internal" --allow tcp:0-65535,udp:0-65535,icmp --network "taw-custom-network" --source-ranges "10.0.0.0/16","10.2.0.0/16","10.1.0.0/16"
```

# 3. ssh

```bash
gcloud compute firewall-rules create "nw101-allow-ssh" --allow tcp:22 --network "taw-custom-network" --target-tags "ssh"
````

# 4. rdp 

```bash
gcloud compute firewall-rules create "nw101-allow-rdp" --allow tcp:3389 --network "taw-custom-network"
````

# VI/ Déploiement de nouvelles instances VM
Afin d'évaluer la connectivité et les performances à travers différentes zones et régions, il est avisé de déployer des instances VM supplémentaires au sein des sous-réseaux que nous avons établis.

```bash
gcloud compute instances create us-test-02 \--subnet subnet-us-west1 \--zone us-west1-b \--machine-type e2-standard-2 \--tags ssh,http# Créer une instance VM dans us-east1gcloud compute instances create us-test-03 \--subnet subnet-us-east1 \--zone us-east1-c \--machine-type e2-standard-2 \--tags ssh,http
````

# VII/ Test de la connectivité et des performances
Suite à la configuration de notre réseau et au déploiement des instances, il est essentiel d'évaluer la connectivité réseau ainsi que les performances. Cette démarche peut englober des tests de ping pour vérifier la connectivité, et l'utilisation d'outils tels qu'iperf pour mesurer la bande passante entre les instances.

# 1. Installation d'iperf pour les tests de performance

sudo apt-get update && sudo apt-get install -y iperf

# 2. Sur l'instance servant de serveur (écoute sur le port par défaut 5001)

iperf -s

# 3. Sur l'instance cliente, pour initier le test

iperf -c <adresse IP interne de l'instance serveur> -t 30

# terraform

# I/ Création du fichier de configuration Terraform

L'exécution de cette commande génère un fichier vide nommé instance.tf. Dans l'environnement Terraform, les fichiers portant l'extension .tf servent à définir l'infrastructure à déployer. En l'occurrence, le fichier instance.tf sera utilisé pour spécifier les détails de déploiement d'une instance de machine virtuelle sur Google Cloud.

```bash
touch instance.tf
```

# II/ Structure du fichier

Le fichier instance.tf contient la configuration d'une ressource de type google_compute_instance, représentant une instance de machine virtuelle dans Google Cloud. Chaque élément de cette configuration est expliqué ci-dessous :

# 1. Initialisation de Terraform

Cette commande initialise le répertoire de travail Terraform en téléchargeant et installant les plugins requis, y compris le fournisseur Google Cloud (google). C'est la première étape à effectuer avant de planifier ou d'appliquer votre configuration.

```bash
terraform init
```
# 2. Planification des modifications

La commande "terraform plan" permet de prévisualiser les modifications que Terraform s'apprête à apporter à l'infrastructure, sans les appliquer. Cela offre une opportunité de vérifier que la configuration spécifiée produit l'effet désiré avant de procéder à l'application des changements.

```bash
terraform plan
```

# 3. Application de la configuration

Après avoir généré le plan d'action avec "terraform plan", l'exécution de ce plan avec "terraform apply" crée ou modifie l'infrastructure réelle sur Google Cloud conformément à la définition spécifiée dans instance.tf. Terraform demande une confirmation avant d'appliquer les changements; répondre "yes" confirmera l'application des modifications.

```bash
terraform apply
```

# 4. Vérification de l'état de l'infrastructure

Une fois la configuration appliquée, "terraform show" permet d'examiner l'état actuel de l'infrastructure gérée par Terraform. Cela inclut les détails de toutes les ressources créées, telles que les instances de machine virtuelle.

```bash
terraform show
```

Lien de mon docker hub : https://hub.docker.com/r/alexandrine1229/mylinux






